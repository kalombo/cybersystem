# -*- coding: utf-8 -*-
import math


class Vector(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def length(self):
        '''
        Длина вектора
        '''
        x = self.x
        y = self.y
        return math.sqrt(x * x + y * y)

    def normalize(self):
        '''
        Метод возвращает нормализированный вектор
        '''
        x = self.x / self.length
        y = self.y / self.length
        return Vector(x, y)

    def coef(self, a):
        '''
        Умножение вектора на число
        '''
        x = self.x * a
        y = self.y * a
        return Vector(x, y)

    def __add__(self, vector):
        x = self.x + vector.x
        y = self.y + vector.y
        return Vector(x, y)

    def __sub__(self, vector):
        x = self.x - vector.x
        y = self.y - vector.y
        return Vector(x, y)

    def __mul__(self, vector):
        '''
        Скалярное произведение векторов
        '''
        return self.x * vector.x + self.y * vector.y

    def __repr__(self):
        return '<%s: %s, %s>' % (self.__class__.__name__, self.x, self.y)


def collision(speed, normal):
    normal = normal.normalize()
    scalar = normal * speed
    return speed - normal.coef(2 * scalar)


def main():
    speed = Vector(1, -1)
    normal = Vector(0, 5)
    print collision(speed, normal)
if __name__ == "__main__":
    main()