# -*- coding: utf-8 -*-
import math


class NullDeterminantException(Exception):
    pass


class Matrix(object):
    def __init__(self, matrix, size=None):
        '''
        Матрицу можно задавать как списком рядов:
        m = Matrix([[1,2], [2,1]])

        Так и линейным списком с указанием размерности:
        m = Matrix([1, 2, 2, 1], 2)
        '''
        #Внутренним представлением квадратной матрицы
        #Будет линейный список и её размерность
        data = []
        #Переводим во внутреннее представление матрицы
        if size is None:
            for lst in matrix:
                data.extend(lst)
            self.data = data
            self.size = len(matrix)
        else:
            self.data = matrix
            self.size = size

    def minor(self, i, j):
        '''
        Минор матрицы
        :param i: номер строки
        :param j: номер столбца
        '''
        size = self.size
        data = self.data
        new_data = []
        for  k in range(size * size):
            if (math.floor(k / size) != i and k % size != j):
                new_data.append(data[k])
        new_matrix = Matrix(new_data, size - 1)
        return new_matrix.determinant

    def addition(self, i, j):
        '''
        Алгебраическое дополнение матрицы
        :param i: номер строки
        :param j: номер столбца
        '''
        return math.pow(-1, i + j) * self.minor(i, j)

    def addition_matrix(self):
        '''
        Матрица алгебраических дополнений
        '''
        new_data = []
        size = self.size
        for  i in xrange(size):
            for  j in xrange(size):
                new_data.append(self.addition(i, j))
        return Matrix(new_data, size)

    def transpose_matrix(self):
        '''
        Транспонированная матрица
        '''

        size = self.size
        data = self.data
        new_data = []
        for  i in range(size):
            for  j in range(size):
                new_data.append(data[j * size + i])
        return Matrix(new_data, size)

    def inversion_matrix(self):
        '''
        Обратная матрица
        '''
        m = self.addition_matrix().transpose_matrix()
        determinant = self.determinant
        if determinant == 0:
            raise NullDeterminantException("Нельзя найти матрицу у которой определитель равен 0")
        for num, el in enumerate(m.data):
            m.data[num] = el / determinant
        return m

    @property
    def determinant(self):
        '''
        Определитель матрицы
        '''

        data = self.data
        size = self.size
        if self.size == 0:
            det = 0
        if self.size == 1:
            det = data[0]
        if self.size == 2:
            det = data[0] * data[3] - data[2] * data[1]
        if self.size == 3:
            det = data[0] * data[4] * data[8] + data[1] * data[5] * data[6] +\
                  data[2] * data[3] * data[7] - data[6] * data[4] * data[2] -\
                  data[7] * data[5] * data[0] - data[8] * data[3] * data[1]
        if self.size > 3:
            det = 0
            i = 0
            for  j in range(self.size):
                det += data[i * size + j] * self.addition(i, j)
        return det

    def get_split_data(self):
        data = self.data
        size = self.size
        return [data[i:i + size] for i in xrange(0, len(data), size)]

    def __mul__(self, matrix):
        '''
        Умножение матриц
        '''

        size = self.size
        self_data = self.get_split_data()
        matrix_data = matrix.get_split_data()

        result_data = []
        for i in xrange(size):
            row = []
            for j in xrange(size):
                row.append(0)
            result_data.append(row)

        for i in xrange(size):
            for j in  xrange(size):
                for k in xrange(size):
                    result_data[i][j] += self_data[i][k] * matrix_data[k][j]
        return Matrix(result_data)

    def __repr__(self):
        data = self.get_split_data()
        for row in data:
            for num, el in enumerate(row):
                row[num] = int(round(el))
        return '\n'.join([str(x) for x in data])


def main():
    m1 = Matrix([[3, 2, 2, 1], [1, 1, 3, 1], [2, 5, 3, 4], [2, 5, 13, 4]])
    print "Определитель:", m1.determinant
    m2 = m1.inversion_matrix()
    print "Обратная матрица"
    print m2
    print "Единичная матрица:"
    print m1 * m2
if __name__ == "__main__":
    main()